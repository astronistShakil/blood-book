package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.sadak.bloodhero.database.BdLocation;
import com.example.sadak.bloodhero.database.DbHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FindDonorActivity extends AppCompatActivity {

    Spinner spDivision, spDistrict, spUpozila, spBloodGroup;

    AppUser appUser = new AppUser();
    ListView lvInformation;
    private FirebaseAuth firebaseAuth;
    //    FirebaseUser user;
    List<AppUser> informationList;
    DatabaseReference ref;
    Button btnOk;
    String[] bloodGroup;

    ArrayList<String> divisions = new ArrayList<>();

    ArrayList<String> districts = new ArrayList<>();

    ArrayList<String> upozillas = new ArrayList<>();

    ArrayList<BdLocation> divisionLocations;
    ArrayList<BdLocation> districtLocations;
    ArrayList<BdLocation> upozillaLocations;

    int divPosition;
    int disPosition;
    int upoPosition;

    BdLocation searchDivision;
    BdLocation searchDistrict;
    BdLocation searchUpozilla;
    String searchBloodGroup;

    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_donor);

        dbHelper = new DbHelper(this);

        divisionLocations = dbHelper.getChildLocations(0);
       // divisions.add(0,"Division");
        for (int i = 0; i <divisionLocations.size(); i++) {
            divisions.add(divisionLocations.get(i).getName());
        }
        //final ArrayList<BdLocation> districtLocations = dbHelper.getChildLocations(searchDivision.getId());
        //final ArrayList<BdLocation> upozillaLocations= dbHelper.getChildLocations(searchDistrict.getId());


        bloodGroup = getResources().getStringArray(R.array.blood_groups);

        spDivision = findViewById(R.id.spinnerDiv);
        spDistrict = findViewById(R.id.spinnerDis);
        spUpozila = findViewById(R.id.spinnerUp);
        spBloodGroup = findViewById(R.id.spinnerBG);
        btnOk = findViewById(R.id.okButton);


        spDivision.setAdapter(new ArrayAdapter<String>(FindDonorActivity.this, android.R.layout.simple_dropdown_item_1line, divisions));
        spDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchDivision = divisionLocations.get(position);
                divPosition = position;

                districts.clear();
                districtLocations = dbHelper.getChildLocations(searchDivision.getId());
                //districts.add(0,"District");
                for (int i = 0; i <districtLocations.size(); i++) {
                    districts.add(districtLocations.get(i).getName());
                }

                spDistrict.setAdapter(new ArrayAdapter<String>(FindDonorActivity.this, android.R.layout.simple_dropdown_item_1line,  districts));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       // spDistrict.setAdapter(new ArrayAdapter<String>(FindDonorActivity.this, android.R.layout.simple_dropdown_item_1line,  districts));
        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchDistrict = districtLocations.get(position);
                disPosition = position;
                upozillas.clear();

                upozillaLocations = dbHelper.getChildLocations(searchDistrict.getId());
               //upozillas.add(0,"Upozilla");
                for(int i = 0; i<upozillaLocations.size();i++)
                {
                    upozillas.add(upozillaLocations.get(i).getName());
                }
                spUpozila.setAdapter(new ArrayAdapter<String>(FindDonorActivity.this, android.R.layout.simple_dropdown_item_1line,  upozillas));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spUpozila.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(upozillaLocations != null) {
                    searchUpozilla = upozillaLocations.get(position);
                    upoPosition = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchBloodGroup = bloodGroup[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        firebaseAuth = FirebaseAuth.getInstance();
//        user = FirebaseAuth.getInstance().getCurrentUser();

        informationList = new ArrayList<>();
        lvInformation = findViewById(R.id.donorListView);
        ref = FirebaseDatabase.getInstance().getReference("donors");

//        showemail.setText("Wellcome to "+ user.getEmail());

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        informationList.clear();
                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            AppUser appUser = userSnapshot.getValue(AppUser.class);

                            if (appUser != null
                                    && appUser.getUserdivision() != null
                                    && appUser.getUserdivision().trim().toLowerCase().equals(searchDivision.getName().trim().toLowerCase())
                                    && appUser.getUserbloodgroup() != null
                                    && appUser.getUserbloodgroup().trim().toLowerCase().equals(searchBloodGroup.trim().toLowerCase())
                                    && appUser.getUserupozila() != null
                                    && appUser.getUserupozila().trim().toLowerCase().equals(searchUpozilla.getName().trim().toLowerCase())
                                    && appUser.getUserdistrict() != null
                                    && appUser.getUserdistrict().trim().toLowerCase().equals(searchDistrict.getName().trim().toLowerCase())) {
                                informationList.add(appUser);
                            }

                        }
                        InformationList adapter = new InformationList(FindDonorActivity.this, informationList);
                        lvInformation.setAdapter(adapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });

        lvInformation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(FindDonorActivity.this, InfoDialogeActivity.class).putExtra("info", informationList.get(i)));
            }
        });


    }
}
