package com.example.sadak.bloodhero;

import java.io.Serializable;

public class AppUser implements Serializable{


    private  String username;
    private  String useremail;
    private  String userdivision;
    private  String userdistrict;
    private  String userupozila;
    private  String userbloodgroup;
    private  String userheight;
    private  String userweight;
    private  String usercontact;
    private  String userpassword;

    public AppUser() {
    }

    public AppUser(String username, String useremail, String userdivision, String userdistrict, String userupozila, String userbloodgroup, String userheight, String userweight, String usercontact, String userpassword) {
        this.username = username;
        this.useremail = useremail;
        this.userdivision = userdivision;
        this.userdistrict = userdistrict;
        this.userupozila = userupozila;
        this.userbloodgroup = userbloodgroup;
        this.userheight = userheight;
        this.userweight = userweight;
        this.usercontact = usercontact;
        this.userpassword = userpassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUserdivision() {
        return userdivision;
    }

    public void setUserdivision(String userdivision) {
        this.userdivision = userdivision;
    }

    public String getUserdistrict() {
        return userdistrict;
    }

    public void setUserdistrict(String userdistrict) {
        this.userdistrict = userdistrict;
    }

    public String getUserupozila() {
        return userupozila;
    }

    public void setUserupozila(String userupozila) {
        this.userupozila = userupozila;
    }

    public String getUserbloodgroup() {
        return userbloodgroup;
    }

    public void setUserbloodgroup(String userbloodgroup) {
        this.userbloodgroup = userbloodgroup;
    }

    public String getUserheight() {
        return userheight;
    }

    public void setUserheight(String userheight) {
        this.userheight = userheight;
    }

    public String getUserweight() {
        return userweight;
    }

    public void setUserweight(String userweight) {
        this.userweight = userweight;
    }

    public String getUsercontact() {
        return usercontact;
    }

    public void setUsercontact(String usercontact) {
        this.usercontact = usercontact;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "username='" + username + '\'' +
                ", useremail='" + useremail + '\'' +
                ", userdivision='" + userdivision + '\'' +
                ", userdistrict='" + userdistrict + '\'' +
                ", userupozila='" + userupozila + '\'' +
                ", userbloodgroup='" + userbloodgroup + '\'' +
                ", userheight='" + userheight + '\'' +
                ", userweight='" + userweight + '\'' +
                ", usercontact='" + usercontact + '\'' +
                ", userpassword='" + userpassword + '\'' +
                '}';
    }
}
