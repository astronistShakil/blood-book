package com.example.sadak.bloodhero.models;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sadak.bloodhero.R;

import java.util.List;

public class RequestManageListAdapter extends ArrayAdapter<RequestHistory> {

    private Activity context;
    private List<RequestHistory> requestManageList;

    public RequestManageListAdapter(Activity context, List<RequestHistory> requestManageListInflater) {
        super(context, R.layout.manage_request_list_view, requestManageListInflater);
        this.context = context;
        this.requestManageList = requestManageListInflater;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = convertView;
        if (listViewItem == null)
            listViewItem = inflater.inflate(R.layout.manage_request_list_view, null, true);

        TextView fromDate = listViewItem.findViewById(R.id.from_tv);
        TextView toDate = listViewItem.findViewById(R.id.to_tv);
        TextView description = listViewItem.findViewById(R.id.description_tv);
        //TextView userContact = listViewItem.findViewById(R.id.contact);
//        Button more = listViewItem.findViewById(R.id.seemore);

        RequestHistory requestManageList = this.requestManageList.get(position);
//        Log.v("ddd", request_manage_list.getDescription());

        fromDate.setText(requestManageList.getStartDate());
        toDate.setText(requestManageList.getEndDate());
        description.setText(requestManageList.getLocation());

        return listViewItem;
    }

}
