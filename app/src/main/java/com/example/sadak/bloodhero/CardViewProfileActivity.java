package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CardViewProfileActivity extends AppCompatActivity {

    GridLayout mainGrid;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    TextView userName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_profile);
        firebaseAuth = FirebaseAuth.getInstance();
        mainGrid = findViewById(R.id.main_Grid);
           // userName = findViewById(R.id.userNameId);
        setSingleEvent(mainGrid);
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        DatabaseReference ref = fdb.getReference("donors");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
       /* if(id == R.id.id_SignOut){
            // sing_out er kaj hobe akhane.
            Toast.makeText(MainActivity.this, "Make an Activity !", Toast.LENGTH_SHORT).show();

        }*/
        return super.onOptionsItemSelected(item);
    }

    private void setSingleEvent(GridLayout mainGrid) {

        for(int i=0;i<mainGrid.getChildCount();i++)
        {

            CardView cardView = (CardView) mainGrid.getChildAt(i);

            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //profile er kaj
                    if(finalI == 0){
                        //Toast.makeText(CardViewProfileActivity.this, "Make an Activity for Profile!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CardViewProfileActivity.this, ProfileActivity.class));

                    }

                    //History er kaj
                    else if (finalI ==1)
                    {
                        //Toast.makeText(CardViewProfileActivity.this, "Make an Activity for History!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CardViewProfileActivity.this, DonationHistory.class));
                    }

                    //Find Donor er kaj
                    else if (finalI ==2)
                    {
                        //Toast.makeText(CardViewProfileActivity.this, "Make an Activity for Find Donor!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CardViewProfileActivity.this, FindDonorActivity.class));
                    }

                    //Request er kaj
                    else if (finalI ==3)
                    {
                        //Toast.makeText(CardViewProfileActivity.this, "Make an Activity for Request!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CardViewProfileActivity.this, DonorRequestActivity.class));
                    }
                    //Settings er kaj
                    else if (finalI ==4)
                    {
                        startActivity(new Intent(CardViewProfileActivity.this, SettingActivity.class));
                    }
                    //Developer er kaj

                    else if (finalI ==5)
                    {
                        startActivity(new Intent(CardViewProfileActivity.this, DeveloperActivity.class));

                    }
                }
            });


        }
    }
}
