package com.example.sadak.bloodhero;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sadak.bloodhero.models.RequestHistory;
import com.example.sadak.bloodhero.models.RequestManageListAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ManageRequestActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    ListView manageReqList;
    FirebaseUser user;
    private static DatabaseReference ref;
    List<RequestHistory> requestManageList;
    RequestManageListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_request);

        firebaseAuth = FirebaseAuth.getInstance();
        requestManageList = new ArrayList<>();
        manageReqList = findViewById(R.id.managerequestlistview);
        manageReqList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                return false;
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        // String userId = user.getEmail().replace("@", "-").replace(".", "-");

        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        ref = fdb.getReference("Request");//.child(userId);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot requestManageList : dataSnapshot.getChildren()) {
                    findChildData(requestManageList.getRef());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void findChildData(final DatabaseReference parentRef) {
        parentRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ManageRequestActivity.this.requestManageList.clear();
                for (DataSnapshot request : dataSnapshot.getChildren()) {
                    RequestHistory requestHistory = request.getValue(RequestHistory.class);
                    requestHistory.setRef(request.getRef());
                    ManageRequestActivity.this.requestManageList.add(requestHistory);
                    Log.v("lslsl", dataSnapshot.getValue().toString());
                }
                adapter = new RequestManageListAdapter(ManageRequestActivity.this, requestManageList);
                manageReqList.setAdapter(adapter);
                Log.v("sass", manageReqList.toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        manageReqList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder adb = new AlertDialog.Builder(ManageRequestActivity.this);
                adb.setTitle("Do you want to delete this request?");
               
                adb.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestManageList.get(position).getRef().setValue(null);
                        adapter.notifyDataSetChanged();
                    }
                });

                adb.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });

                adb.create().show();

                       
                return false;
            }

        });


    }

    public static class RequestDeleteDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Do you want to delete it ?")
                    .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // delete here
                            int pos = getArguments().getInt("pos");
                            RequestHistory history = (RequestHistory) getArguments().getSerializable("req");
                            history.getRef().setValue(null);
                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}
