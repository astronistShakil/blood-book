package com.example.sadak.bloodhero.database;

public class BdLocation {
    int id;
    String name;
    String category;
    int parent;

    public BdLocation(int id, String name, String category, int parent) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.parent = parent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "BdLocation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", parent=" + parent +
                '}';
    }
}
