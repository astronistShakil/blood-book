package com.example.sadak.bloodhero;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class InformationList extends ArrayAdapter<AppUser> {

    private Activity context;
    private List<AppUser> informationlist;


    public InformationList(Activity context, List<AppUser> informationList) {
        super(context, R.layout.datalist, informationList);
        this.context = context;
        this.informationlist = informationList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = convertView;
        if (listViewItem == null) listViewItem = inflater.inflate(R.layout.datalist, null, true);

        TextView userName = listViewItem.findViewById(R.id.name);
        TextView userBlood = listViewItem.findViewById(R.id.bloodgroup);
        TextView userContact = listViewItem.findViewById(R.id.contact);
//        Button more = listViewItem.findViewById(R.id.seemore);

        AppUser appUser = informationlist.get(position);
        userName.setText(appUser.getUsername());
        userBlood.setText(appUser.getUserbloodgroup());
        userContact.setText(appUser.getUsercontact());
        return listViewItem;
    }
}

