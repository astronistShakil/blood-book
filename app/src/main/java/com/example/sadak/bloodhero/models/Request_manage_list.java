package com.example.sadak.bloodhero.models;

public class Request_manage_list {

    private String fromDate, toDate, description;

    public Request_manage_list() {

    }

    public Request_manage_list(String fromDate, String toDate, String description) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.description = description;
    }

    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public String getDescription() {
        return description;
    }
}
