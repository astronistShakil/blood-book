package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class AdminWorksActivity extends AppCompatActivity {

    Button btnManageRequest,btnManageUser;
    private FirebaseAuth firebaseAuth;
    Button btnlogOut;

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),AdminLoginActivity.class));
        finish();
        super.onBackPressed();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_works);
        firebaseAuth = FirebaseAuth.getInstance();
        btnManageRequest = findViewById(R.id.btManageRequest);
        btnManageUser = findViewById(R.id.btManageUser);

        btnManageRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminWorksActivity.this,ManageRequestActivity.class));
            }
        });

        btnManageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminWorksActivity.this,ManageUserActivity.class));


            }
        });

        btnlogOut = findViewById(R.id.btLogOut);
        btnlogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();

                startActivity(new Intent(getApplicationContext(),AdminLoginActivity.class));
                finish();
            }
        });




    }
}
