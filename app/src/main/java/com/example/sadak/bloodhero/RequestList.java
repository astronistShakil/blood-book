package com.example.sadak.bloodhero;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.sadak.bloodhero.models.RequestHistory;

import java.util.List;

public class RequestList extends ArrayAdapter<RequestHistory> {

    private Activity context;
    private List<RequestHistory> requestList;
    private OnItemClickListener itemClickListener;

    public RequestList(Activity context, List<RequestHistory> RequestList, OnItemClickListener onItemClickListener) {
        super(context, R.layout.request_data,RequestList);
        this.context = context;
        this.requestList = RequestList;
        this.itemClickListener=onItemClickListener;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = convertView;
        if (listViewItem == null) listViewItem = inflater.inflate(R.layout.request_data, null, true);

        TextView descriptionPlace = listViewItem.findViewById(R.id.description_tv);
        TextView startDate = listViewItem.findViewById(R.id.from_tv);
        TextView endDate = listViewItem.findViewById(R.id.to_tv);
        TextView deFault= listViewItem.findViewById(R.id.default_tv);
        Button btnRemove= listViewItem.findViewById(R.id.btRemove);
        Button btnAccept= listViewItem.findViewById(R.id.btAccept);

        final RequestHistory requestHistory = requestList.get(position);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.onItemDeleteAction(position,requestList);
//                    requestHistory.getKey();
                }
            }
        });
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.onItemAcceptAction(requestHistory.getUserKey());
                }
            }
        });

        descriptionPlace.setText(requestHistory.getLocation());
        startDate.setText(requestHistory.getStartDate());
        endDate.setText(requestHistory.getEndDate());
        if(!requestHistory.isCanRemove()){
            btnRemove.setVisibility(View.INVISIBLE);
            btnAccept.setVisibility(View.VISIBLE);
        }else {
            btnRemove.setVisibility(View.VISIBLE);
            btnAccept.setVisibility(View.INVISIBLE);
        }

        return listViewItem;
    }

    public interface OnItemClickListener{
        void onItemDeleteAction(int position, List<RequestHistory> requestList);
        void onItemAcceptAction(String donerUserId);
    }

}
