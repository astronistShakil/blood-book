package com.example.sadak.bloodhero;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AdminLoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAdminLogin;
    EditText logemail;
    EditText logpass;
    ProgressDialog progressDialog;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        btnAdminLogin = findViewById(R.id.btAdminLogin);
        logemail = findViewById(R.id.loginemail);
        logpass = findViewById(R.id.loginpass);
        firebaseAuth =firebaseAuth.getInstance();
        btnAdminLogin.setOnClickListener(this);

        if(firebaseAuth.getCurrentUser() != null){


            startActivity(new Intent(getApplicationContext(),AdminWorksActivity.class));
            finish();
        }
        progressDialog = new ProgressDialog(this);


    }
    private void adminLogin()
    {
        String email = logemail.getText().toString().trim();
        String password = logpass.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(AdminLoginActivity.this, "Enter the Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(AdminLoginActivity.this, "Enter the Pasword", Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("please wait a while..");
        progressDialog.show();

        if(email.equalsIgnoreCase("admin@gmail.com")){
            firebaseAuth.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            if(task.isSuccessful()){
                                finish();
                                startActivity(new Intent(getApplicationContext(),AdminWorksActivity.class));
                            }
                            else{
                                Toast.makeText(AdminLoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        if(v == btnAdminLogin)
        {
            adminLogin();
        }

    }
}
