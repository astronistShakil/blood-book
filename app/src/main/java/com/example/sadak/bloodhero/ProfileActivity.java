package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextView showingEmail;
    Button signout;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    TextView  tvName,tvEmail, tvDivision, tvDistrict, tvBloodgroup, tvUpozila, tvContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilepage);
        firebaseAuth = FirebaseAuth.getInstance();

        tvName = findViewById(R.id.textView3);
        tvEmail = findViewById(R.id.textView4);
        tvDivision = findViewById(R.id.textView5);
        tvDistrict = findViewById(R.id.textView6);
        tvUpozila = findViewById(R.id.textView7);
        tvBloodgroup = findViewById(R.id.textView8);
        tvContact = findViewById(R.id.textView9);


        showingEmail = findViewById(R.id.tvuseremail);
        signout = findViewById(R.id.logoutB);

        signout.setOnClickListener(this);


        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(getApplicationContext(),EditProfile.class));
        }
        user = firebaseAuth.getCurrentUser();
        final String userId = user.getEmail().replace("@", "-").replace(".", "-");


        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        final DatabaseReference ref = fdb.getReference("donors");


        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AppUser appUser = dataSnapshot.getValue(AppUser.class);
                if (appUser.getUseremail().equals(user.getEmail())) {
                    tvName.setText(appUser.getUsername());
                    tvEmail.setText(appUser.getUseremail());
                    tvDivision.setText(appUser.getUserdivision());
                    tvDistrict.setText(appUser.getUserdistrict());
                    tvUpozila.setText(appUser.getUserupozila());
                    tvBloodgroup.setText(appUser.getUserbloodgroup());
                    tvContact.setText(appUser.getUsercontact());
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        showingEmail.setText("Wellcome to "+ user.getEmail());
        /*((Button)findViewById(R.id.btnUpdate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref.child(userId).child("username").setValue(tvName.getText().toString().trim());
            }
        });*/

    }

    @Override
    public void onClick(View view) {
        if (view == signout)
        {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(getApplicationContext(),homepage.class));
        }

    }


}