package com.example.sadak.bloodhero;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoDialogeActivity extends AppCompatActivity {
    TextView tvEmail,tvDivision,tvDistrict,tvUpozila;
    Button btnCall,btnMassege;
    AppUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_dialoge);

        user = (AppUser) getIntent().getSerializableExtra("info");

        tvEmail = findViewById(R.id.EmailInfo);
        tvDivision = findViewById(R.id.DivisionInfo);
        tvDistrict = findViewById(R.id.DistrictInfo);
        tvUpozila = findViewById(R.id.UpozilaInfo);
        btnCall = findViewById(R.id.CallButton);
        btnMassege = findViewById(R.id.MassButton);


        tvEmail.setText("Email: "+ user.getUseremail());
        tvDivision.setText("Division: "+ user.getUserdivision());
        tvDistrict.setText("District: "+ user.getUserdistrict());
        tvUpozila.setText("Upozila: "+ user.getUserupozila());

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+Uri.encode(user.getUsercontact()))));
            }
        });

        btnMassege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto: "+ Uri.encode(user.getUsercontact()))));
            }
        });
    }
}
