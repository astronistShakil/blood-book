package com.example.sadak.bloodhero;

import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sadak.bloodhero.database.BdLocation;
import com.example.sadak.bloodhero.database.DbHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import static com.example.sadak.bloodhero.R.*;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {
    private Button RG;
    private EditText name;
    private EditText email;
    private EditText height;
    private EditText weight;
    private EditText contact;
    private EditText password;

    AppUser appUser = new AppUser();

    DonationHistory donationHistory = new DonationHistory();

    private Spinner spDivision, spDistrict, spUpozila, spBloodGroup;

    ArrayList<String> divisions = new ArrayList<>();

    ArrayList<String> districts = new ArrayList<>();

    ArrayList<String> upozillas = new ArrayList<>();

    ArrayList<BdLocation> divisionLocations;
    ArrayList<BdLocation> districtLocations;
    ArrayList<BdLocation> upozillaLocations;

    int divPosition;
    int disPosition;
    int upoPosition;

    BdLocation searchDivision;
    BdLocation searchDistrict;
    BdLocation searchUpozilla;
    String searchBloodGroup;

    DbHelper dbHelper;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    FirebaseDatabase fdb;
    String [] upozila;
    String [] bloodGroup;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        //upozila = getResources().getStringArray(array.upozilaInDhaka);
       bloodGroup = getResources().getStringArray(array.blood_groups);

        spDivision = findViewById(R.id.spinnerDiv);
        spDistrict = findViewById(R.id.spinnerDis);
        spUpozila = findViewById(R.id.spinnerUp);
        spBloodGroup = findViewById(R.id.spinnerBG);


        fdb = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        RG = findViewById(R.id.register2);
        name = findViewById(R.id.et1);
        email = findViewById(R.id.et2);
        height = findViewById(R.id.et7);
        weight = findViewById(R.id.et8);
        contact = findViewById(R.id.et9);
        password = findViewById(R.id.et10);
        dbHelper = new DbHelper(this);

        divisionLocations = dbHelper.getChildLocations(0);
        for (int i = 0; i < divisionLocations.size(); i++) {
            divisions.add(divisionLocations.get(i).getName());
        }
        //final ArrayList<BdLocation> districtLocations = dbHelper.getChildLocations(searchDivision.getId());
        //final ArrayList<BdLocation> upozillaLocations= dbHelper.getChildLocations(searchDistrict.getId());
        spDivision.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_dropdown_item_1line, divisions));
        spDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchDivision = divisionLocations.get(position);
                divPosition = position;

                districts.clear();
                districtLocations = dbHelper.getChildLocations(searchDivision.getId());
                for (int i = 0; i < districtLocations.size(); i++) {
                    districts.add(districtLocations.get(i).getName());
                }

                spDistrict.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_dropdown_item_1line,  districts));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // spDistrict.setAdapter(new ArrayAdapter<String>(FindDonorActivity.this, android.R.layout.simple_dropdown_item_1line,  districts));
        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchDistrict = districtLocations.get(position);
                disPosition = position;
                upozillas.clear();

                upozillaLocations = dbHelper.getChildLocations(searchDistrict.getId());
                for(int i = 0; i<upozillaLocations.size();i++)
                {
                    upozillas.add(upozillaLocations.get(i).getName());
                }
                spUpozila.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_dropdown_item_1line,  upozillas));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spUpozila.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(upozillaLocations != null) {
                    searchUpozilla = upozillaLocations.get(position);
                    upoPosition = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchBloodGroup = bloodGroup[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        RG.setOnClickListener(this);

    }

    private void registerUser() {
        String Email = email.getText().toString().trim();
        String Password = password.getText().toString().trim();

        if (TextUtils.isEmpty(Email)) {
            Toast.makeText(EditProfile.this, "Enter the Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(Password)) {
            Toast.makeText(EditProfile.this, "Enter the Pasword", Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("please wait a while..");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            storeUserData();
                        } else {
                            Log.e("RegistrationException", "NOT registered " + task.getException());
                            Toast.makeText(EditProfile.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                });

    }

    private void storeUserData() {
        appUser.setUserdivision(spDivision.getSelectedItem().toString().trim());
        appUser.setUserdistrict(spDistrict.getSelectedItem().toString().trim());
        appUser.setUserupozila(spUpozila.getSelectedItem().toString().trim());
        appUser.setUserbloodgroup(spBloodGroup.getSelectedItem().toString().trim());
        appUser.setUsername(name.getText().toString().trim());
        appUser.setUseremail(email.getText().toString().trim());
        appUser.setUserheight(height.getText().toString().trim());
        appUser.setUserweight(weight.getText().toString().trim());
        appUser.setUsercontact(contact.getText().toString().trim());
        appUser.setUserpassword(password.getText().toString().trim());

        DatabaseReference userRef = fdb.getReference("donors");
        userRef.child(appUser.getUseremail().replace("@", "-").replace(".", "-")).setValue(appUser);


        //DatabaseReference userRef1 = fdb.getReference("History");
       // userRef1.child(appUser.getUseremail().replace("@", "-").replace(".", "-")).push().setValue(donationHistory);

        progressDialog.dismiss();
        Toast.makeText(EditProfile.this, "Successfully registered", Toast.LENGTH_SHORT).show();
        finish();
        startActivity(new Intent(getApplicationContext(), CardViewProfileActivity.class));
    }

    @Override
    public void onClick(View view) {
        if (view == RG) {
            registerUser();

        }
    }
}
