package com.example.sadak.bloodhero;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.sadak.bloodhero.database.BdLocation;
import com.example.sadak.bloodhero.database.DbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        geoDataHandler();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, homepage.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void geoDataHandler() {
        dbHelper = new DbHelper(this);

        if(dbHelper.isDataAvailable()){
            return;
        }

        try {
            String jsonData = readJson();
            parseAndInsertToDb(jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String readJson() throws IOException {
        InputStream is = getResources().openRawResource(R.raw.bangladesh_data);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];

        Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        int n;
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }

        is.close();

        return writer.toString();
    }

    private void parseAndInsertToDb(String jsonData) throws JSONException {
        JSONObject root = new JSONObject(jsonData);

        // Division
        JSONArray divisions = root.getJSONArray("divisions");
        for (int i = 0; i < divisions.length(); i++) {
            JSONObject division = divisions.getJSONObject(i);
            dbHelper.insertLocation(new BdLocation(
                    division.getInt("id"),
                    division.getString("name"),
                    "div",
                    0
            ));
        }

        // District
        JSONArray districts = root.getJSONArray("districts");
        for (int i = 0; i < districts.length(); i++) {
            JSONObject district = districts.getJSONObject(i);
            dbHelper.insertLocation(new BdLocation(
                    district.getInt("id")+divisions.length()+1,
                    district.getString("name"),
                    "dist",
                    district.getInt("division_id")
            ));
        }

        // Upazilla
        JSONArray upazillas = root.getJSONArray("upazillas");
        for (int i = 0; i < upazillas.length(); i++) {
            JSONObject upazilla = upazillas.getJSONObject(i);
            BdLocation bdLocation = new BdLocation(
                    upazilla.getInt("id")+districts.length()+1,
                    upazilla.getString("name"),
                    "dist",
                    upazilla.getInt("district_id")+divisions.length()+1
            );
            dbHelper.insertLocation(bdLocation);

            Log.e("PD_DEBUG",bdLocation.toString());
        }
    }
}
