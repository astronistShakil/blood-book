package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SettingActivity extends AppCompatActivity {

    EditText name, email,division,district,upozilla,bloodGroup,height,weight,contact,password;
    Button btnUpdate;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        name = findViewById(R.id.etName);
        email = findViewById(R.id.etEmail);
        division = findViewById(R.id.etDiv);
        district = findViewById(R.id.etDis);
        upozilla = findViewById(R.id.etUpo);
        bloodGroup = findViewById(R.id.etBloodGp);
        height = findViewById(R.id.etHieght);
        weight = findViewById(R.id.etWeight);
        contact = findViewById(R.id.etContact);
        password = findViewById(R.id.etPassword);
        btnUpdate = findViewById(R.id.btUpdate);


        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        final String userId = user.getEmail().replace("@", "-").replace(".", "-");
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        final DatabaseReference ref = fdb.getReference("donors");

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AppUser appUser = dataSnapshot.getValue(AppUser.class);
                if (appUser.getUseremail().equals(user.getEmail())) {
                    name.setText(appUser.getUsername());
                    email.setText(appUser.getUseremail());
                    division.setText(appUser.getUserdivision());
                    district.setText(appUser.getUserdistrict());
                    upozilla.setText(appUser.getUserupozila());
                    bloodGroup.setText(appUser.getUserbloodgroup());
                    height.setText(appUser.getUserheight());
                    weight.setText(appUser.getUserweight());
                    contact.setText(appUser.getUsercontact());
                    password.setText(appUser.getUserpassword());
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref.child(userId).child("username").setValue(name.getText().toString().trim());
                ref.child(userId).child("useremail").setValue(email.getText().toString().trim());
                ref.child(userId).child("userdivision").setValue(division.getText().toString().trim());
                ref.child(userId).child("userdistrict").setValue(district.getText().toString().trim());
                ref.child(userId).child("userupozila").setValue(upozilla.getText().toString().trim());
                ref.child(userId).child("userbloodgroup").setValue(bloodGroup.getText().toString().trim());
                ref.child(userId).child("userheight").setValue(height.getText().toString().trim());
                ref.child(userId).child("userweight").setValue(weight.getText().toString().trim());
                ref.child(userId).child("usercontact").setValue(contact.getText().toString().trim());
                ref.child(userId).child("userpassword").setValue(password.getText().toString().trim());

                Toast.makeText(SettingActivity.this, "Successfully updated !", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(getApplicationContext(), CardViewProfileActivity.class));
            }
        });


    }
}
