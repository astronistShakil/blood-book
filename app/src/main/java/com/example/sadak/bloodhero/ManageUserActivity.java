package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ManageUserActivity extends AppCompatActivity {

    ListView manageUserList;
    private FirebaseAuth firebaseAuth;
      FirebaseUser user;
    List<AppUser> userInformation;
    DatabaseReference ref;
    //AppUser user;
    private String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_user);

        firebaseAuth = FirebaseAuth.getInstance();
        userInformation = new ArrayList<>();
        manageUserList = findViewById(R.id.manageuserlistview);
        ref = FirebaseDatabase.getInstance().getReference("donors");
//        userId = user.getEmail().replace("@","-").replace(".","-");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userInformation.clear();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    AppUser appUser = userSnapshot.getValue(AppUser.class);
                    userInformation.add(appUser);
                }
                InformationList adapter = new InformationList(ManageUserActivity.this, userInformation);
                manageUserList.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        manageUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(ManageUserActivity.this, DelateableUserInfoActivity.class).putExtra("info", userInformation.get(i)));
                }
        });

    }
}
