package com.example.sadak.bloodhero;

import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Loginpage extends AppCompatActivity implements View.OnClickListener {
        Button LG;
        EditText logemail;
        EditText logpass;
        ProgressDialog progressDialog;
        FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginpage);

        LG = this.<Button>findViewById(R.id.loginbutton);
        logemail = findViewById(R.id.loginemail);
        logpass = findViewById(R.id.loginpass);
        firebaseAuth =firebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null){

            finish();
            startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
        }
        progressDialog = new ProgressDialog(this);

        LG.setOnClickListener(this);
    }
        private void userLogin(){

         String email = logemail.getText().toString().trim();
            String password = logpass.getText().toString().trim();

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(Loginpage.this, "Enter the Email", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(Loginpage.this, "Enter the Pasword", Toast.LENGTH_SHORT).show();
                return;
            }
            progressDialog.setMessage("please wait a while..");
            progressDialog.show();

//            if(email.equalsIgnoreCase("admin@gmail.com")){
                firebaseAuth.signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressDialog.dismiss();
                                if(task.isSuccessful()){
                                    finish();
                                    startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
                                }
                                else{
                                    Toast.makeText(Loginpage.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                        });
//            }
        }
    @Override
    public void onClick(View view) {
        if(view == LG){
            userLogin();
        }
    }
}
