package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GuestActivity extends AppCompatActivity {
    TextView showemail;
    Button logout;
    ListView lvInformation;
    private FirebaseAuth firebaseAuth;
    //    FirebaseUser user;
    List<AppUser> informationList;
    DatabaseReference ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        firebaseAuth = FirebaseAuth.getInstance();
//        user = FirebaseAuth.getInstance().getCurrentUser();

        showemail = findViewById(R.id.showemailid);
        informationList = new ArrayList<>();
        lvInformation = findViewById(R.id.listviewid);
        ref = FirebaseDatabase.getInstance().getReference("donors");

//        showemail.setText("Wellcome to "+ user.getEmail());


        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                informationList.clear();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    AppUser appUser = userSnapshot.getValue(AppUser.class);
                    informationList.add(appUser);
                }
                InformationList adapter = new InformationList(GuestActivity.this, informationList);
                lvInformation.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        lvInformation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(GuestActivity.this, InfoDialogeActivity.class).putExtra("info", informationList.get(i)));
            }
        });
    }
}
