package com.example.sadak.bloodhero.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, "geo_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table bangladesh(id integer PRIMARY KEY AUTOINCREMENT, name text, category text, parent integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertLocation(BdLocation location) {
        ContentValues cv = new ContentValues();

        cv.put("id", location.getId());
        cv.put("name", location.getName());
        cv.put("category", location.getCategory());
        cv.put("parent", location.getParent());

        getWritableDatabase().insert("bangladesh", null, cv);
    }

    public BdLocation getLocation(int id) {
        Cursor cursor = getReadableDatabase().query("bangladesh", null, "id = ?", new String[]{id + ""}, null, null, "name");
        cursor.moveToFirst();
        BdLocation bdLocation = new BdLocation(
                cursor.getInt(cursor.getColumnIndex("id")),
                cursor.getString(cursor.getColumnIndex("name")),
                cursor.getString(cursor.getColumnIndex("category")),
                cursor.getInt(cursor.getColumnIndex("parent"))
        );
        cursor.close();
        return bdLocation;
    }

    public ArrayList<BdLocation> getChildLocations(int parentId) {
        ArrayList<BdLocation> list = new ArrayList<>();
        Cursor cursor = getReadableDatabase().query("bangladesh", null, "parent = ?", new String[]{parentId + ""}, null, null, "name");
        while (cursor.moveToNext()) {
            list.add(new BdLocation(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("name")),
                    cursor.getString(cursor.getColumnIndex("category")),
                    cursor.getInt(cursor.getColumnIndex("parent"))
            ));
        }
        return list;
    }

    public boolean isDataAvailable() {
        Cursor cursor = getReadableDatabase().query("bangladesh", null, null, null, null, null, null);
        return cursor.getCount() > 0;
    }
}
