package com.example.sadak.bloodhero.models;

public class Blood_Request {

    private String dateFrom;
    private String dateTo;
    private String description;

    public Blood_Request(){

    }

    public Blood_Request(String dateFrom, String dateTo, String description) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.description = description;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public String getDescription() {
        return description;
    }
}
