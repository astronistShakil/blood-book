package com.example.sadak.bloodhero;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sadak.bloodhero.models.RequestHistory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DonorRequestActivity extends AppCompatActivity implements RequestList.OnItemClickListener {

    private static final String TAG = "DonorRequestActivity";
    private DatePickerDialog.OnDateSetListener tvDateFrom,tvDateTo;
    EditText description;
    TextView from,to;
    Button btPost;

    ListView lvRequestList;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    List<RequestHistory> requestList;

    String dateselect,dateselect1;
    RequestList lvAdapter;
    DatabaseReference ref;
    FirebaseDatabase fdb;
    private String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donor_request);
            requestList = new ArrayList<>();


        from = findViewById(R.id.tvFrom);
        to = findViewById(R.id.tvTo);
//        bloodGroup = findViewById(R.id.edit_group);
        description = findViewById(R.id.editdescription);
        btPost = findViewById(R.id.buttonpost);

        lvRequestList = findViewById(R.id.listviewrequest);

        firebaseAuth = FirebaseAuth.getInstance();
        fdb = FirebaseDatabase.getInstance();
        ref = fdb.getReference("Request");
        user= firebaseAuth.getCurrentUser();
        userId = user.getEmail().replace("@","-").replace(".","-");
        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(DonorRequestActivity.this,
                        android.R.style.Theme_DeviceDefault,tvDateFrom,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                dialog.show();
            }
        });

        tvDateFrom = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
               Log.d(TAG,"onDateSet : etDate : "+i + "/" + (i1+1) +"/" + i2);
                dateselect = i+ "/" + (i1+1) + "/" + i2;
               from.setText(dateselect);
            }
        };

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(DonorRequestActivity.this,
                        android.R.style.Theme_DeviceDefault,tvDateTo,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                dialog.show();
            }
        });

        tvDateTo = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Log.d(TAG,"onDateSet : etDate : "+i + "/" + (i1+1) +"/" + i2);
                dateselect1 = i+ "/" + (i1+1) + "/" + i2;
                to.setText(dateselect1);
            }
        };

        lvAdapter = new RequestList(DonorRequestActivity.this, requestList,this);
        lvRequestList.setAdapter(lvAdapter);


        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
               
                for(DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    RequestHistory request_history = childSnapshot.getValue(RequestHistory.class);
                    request_history.setKey(childSnapshot.getKey());
                    request_history.setUserKey(dataSnapshot.getKey());
                    if(dataSnapshot.getKey().equalsIgnoreCase(userId) && request_history.getSoftDelete() == 0) {
                        request_history.setCanRemove(true);
                        requestList.add(request_history);
                    } else if (!dataSnapshot.getKey().equalsIgnoreCase(userId)) {
                        requestList.add(request_history);
                    }
                    lvAdapter.notifyDataSetChanged();
                   // Log.v("sas", lvRequestList.toString());
                }
                //Log.v("sasa", requestList.toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = firebaseAuth.getCurrentUser();
                String userId = user.getEmail().replace("@","-").replace(".","-");

                String descriptions = description.getText().toString().trim();
                ref.child(userId).push().setValue(new RequestHistory(dateselect,dateselect1,descriptions,0)).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        if (task.isSuccessful()) {
                            startActivity(getIntent());
                            finish();
                            Toast.makeText(DonorRequestActivity.this, "Request added successfully.", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DonorRequestActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                });


                from.setText("From");
                to.setText("To");
                description.setText("");
            }


        });


    }

    @Override
    public void onItemDeleteAction(final int position, final List<RequestHistory> requestList) {
        int size = requestList.size();
        if(size>position){
            user = firebaseAuth.getCurrentUser();
            String userId = user.getEmail().replace("@","-").replace(".","-");

            ref.child(userId).child(requestList.get(position).getKey()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(DonorRequestActivity.this, "Request removed successfully.", Toast.LENGTH_SHORT).show();
                        requestList.remove(position);
                        lvAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(DonorRequestActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }



    }

    @Override
    public void onItemAcceptAction(final String userId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("donors");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    AppUser appUser = userSnapshot.getValue(AppUser.class);
                    if(userSnapshot.getKey().equalsIgnoreCase(userId)){
                        startActivity(new Intent(DonorRequestActivity.this, InfoDialogeActivity.class).putExtra("info", appUser));
                        break;
                    }
                    Log.d(TAG, "onItemAcceptAction() called with:" + userSnapshot.getKey()+ userId +" = [" + userId + "]");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
