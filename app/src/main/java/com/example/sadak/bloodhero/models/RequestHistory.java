package com.example.sadak.bloodhero.models;

import com.google.firebase.database.DatabaseReference;

import java.io.Serializable;

public class RequestHistory implements Serializable{
    String  userKey;
    String key;
    String startDate;
    String endDate;
    String location;
    int softDelete;
    boolean canRemove;
    DatabaseReference ref;

    public DatabaseReference getRef() {
        return ref;
    }

    public void setRef(DatabaseReference ref) {
        this.ref = ref;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public boolean isCanRemove() {
        return canRemove;
    }

    public void setCanRemove(boolean canRemove) {
        this.canRemove = canRemove;
    }

    public int getSoftDelete() {
        return softDelete;
    }

    public void setSoftDelete(int softDelete) {
        this.softDelete = softDelete;
    }

    public RequestHistory(){

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public RequestHistory(String key, String startDate, String endDate, String location, int softDelete) {
        this.key = key;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.softDelete = softDelete;
    }

    public RequestHistory(String key, String startDate, String endDate, String location) {
        this.key = key;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
    }

    public RequestHistory(String startDate, String endDate, String location, int softDelete) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.softDelete = softDelete;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public String getEndDate() {

        return endDate;
    }

    public String getLocation()
    {
        return location;
    }
}
