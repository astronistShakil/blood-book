package com.example.sadak.bloodhero;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sadak.bloodhero.models.Donation_History;

import java.util.List;

public class HistoryList extends ArrayAdapter<Donation_History> {

    private Activity context;
    private List<Donation_History> historyList;

    public HistoryList(Activity context, List<Donation_History> HistoryList) {
        super(context, R.layout.history_data,HistoryList);
        this.context = context;
        this.historyList = HistoryList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = convertView;
        if (listViewItem == null) listViewItem = inflater.inflate(R.layout.history_data, null, true);

        TextView donateLocation = listViewItem.findViewById(R.id.location_tv);
        TextView donateDate = listViewItem.findViewById(R.id.date_tv);
        //TextView userContact = listViewItem.findViewById(R.id.contact);
//        Button more = listViewItem.findViewById(R.id.seemore);

        Donation_History donationHistory = historyList.get(position);

        donateLocation.setText(donationHistory.getLocation());
        donateDate.setText(donationHistory.getDate());

        return listViewItem;
    }
}
