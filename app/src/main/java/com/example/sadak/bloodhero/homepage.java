package com.example.sadak.bloodhero;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class homepage extends AppCompatActivity implements View.OnClickListener {
    Button LG,guest;
    EditText logemail;
    EditText logpass;
    ProgressDialog progressDialog;
    FirebaseAuth firebaseAuth;
    TextView register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        LG = this.<Button>findViewById(R.id.loginbutton);
        guest = findViewById(R.id.guestbutton);
        logemail = findViewById(R.id.loginemail);
        register = findViewById(R.id.registertext);
        logpass = findViewById(R.id.loginpass);
        firebaseAuth =firebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null){

            finish();
            startActivity(new Intent(getApplicationContext(),CardViewProfileActivity.class));
        }
        progressDialog = new ProgressDialog(this);

        LG.setOnClickListener(this);
        guest.setOnClickListener(this);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent= new Intent(homepage.this,EditProfile.class);
                startActivity(intent);
            }
        });
    }
    private void userLogin(){

        String email = logemail.getText().toString().trim();
        String password = logpass.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(homepage.this, "Enter the Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(homepage.this, "Enter the Pasword", Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("please wait a while..");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if(task.isSuccessful()){
                            finish();
                            startActivity(new Intent(getApplicationContext(),CardViewProfileActivity.class));
                        }
                        else{
                            Toast.makeText(homepage.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });
    }
    @Override
    public void onClick(View view) {
        if(view == LG){
            userLogin();
        }
        else if(view == guest )
        {

            startActivity(new Intent(homepage.this,AdminLoginActivity.class));
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
