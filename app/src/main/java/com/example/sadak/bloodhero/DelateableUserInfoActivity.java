package com.example.sadak.bloodhero;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DelateableUserInfoActivity extends AppCompatActivity {
    TextView tvEmail,tvDivision,tvDistrict,tvUpozila,tvHeight,tvWeight;
    Button btnDelate;
    AppUser user;
    FirebaseUser user1;

    DatabaseReference ref;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delateable_user_info);
        ref = FirebaseDatabase.getInstance().getReference("donors");


        user = (AppUser) getIntent().getSerializableExtra("info");
        tvEmail = findViewById(R.id.EmailInfo);
        tvDivision = findViewById(R.id.DivisionInfo);
        tvDistrict = findViewById(R.id.DistrictInfo);
        tvUpozila = findViewById(R.id.UpozilaInfo);
        tvHeight = findViewById(R.id.HeightInfo);
        tvWeight = findViewById(R.id.WeightInfo);
        btnDelate = findViewById(R.id.UserInfoDelate);

        tvEmail.setText("Email: "+ user.getUseremail());
        tvDivision.setText("Division: "+ user.getUserdivision());
        tvDistrict.setText("District: "+ user.getUserdistrict());
        tvUpozila.setText("Upozila: "+ user.getUserupozila());
        tvHeight.setText("Height: "+user.getUserheight());
        tvWeight.setText("Weight: "+user.getUserweight());
         userId = user.getUseremail().replace("@","-").replace(".","-");
        Log.v("aaa", userId);


        btnDelate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           ref.child(userId).setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(DelateableUserInfoActivity.this, "User delated successfully.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DelateableUserInfoActivity.this, ManageUserActivity.class));
                    finish();

                }
            });
            }
        });
    }
}
