package com.example.sadak.bloodhero;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sadak.bloodhero.models.Donation_History;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DonationHistory extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "DonorRequestActivity";
    private DatePickerDialog.OnDateSetListener tvDate;
    EditText etDescription;
    TextView etDate;
    Button btPost,btShow;
    ListView lvInformation;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    List<Donation_History> HistoryList;

    String dateSelect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_history);

        firebaseAuth = FirebaseAuth.getInstance();
        HistoryList = new ArrayList<>();

        etDate = findViewById(R.id.edit_date);
        etDescription = findViewById(R.id.edit_description);
        btPost = findViewById(R.id.button_post);
        lvInformation = findViewById(R.id.listviewrequest);
        btShow = findViewById(R.id.button_show);

        btPost.setOnClickListener(this);


        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(DonationHistory.this,
                        android.R.style.Theme_DeviceDefault,tvDate,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                dialog.show();
            }
        });

        tvDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Log.d(TAG,"onDateSet : etDate : "+i + "/" + (i1+1) +"/" + i2);
                dateSelect = i+ "/" + (i1+1) + "/" + i2;
                etDate.setText(dateSelect);
            }
        };

        btShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth = FirebaseAuth.getInstance();
                user = firebaseAuth.getCurrentUser();
                String userId = user.getEmail().replace("@", "-").replace(".", "-");

                FirebaseDatabase fdb = FirebaseDatabase.getInstance();
                DatabaseReference ref = fdb.getReference("History").child(userId);

                ref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        Donation_History donation_history = dataSnapshot.getValue(Donation_History.class);
                        HistoryList.add(donation_history);
                        HistoryList adapter = new HistoryList(DonationHistory.this, HistoryList);
                        lvInformation.setAdapter(adapter);
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }
        });


    }

    @Override
    public void onClick(View v) {
        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        String userId = user.getEmail().replace("@", "-").replace(".", "-");

        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        DatabaseReference ref = fdb.getReference("History");

        String description = etDescription.getText().toString().trim();
        ref.child(userId).push().setValue(new Donation_History(dateSelect, description)).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(DonationHistory.this, "History added successfully.", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(DonationHistory.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        etDate.setText("");
        etDescription.setText("");
    }
}
